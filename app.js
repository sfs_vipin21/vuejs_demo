// new Vue({
//     el: '#vue-app',
//     data: {
//         name: 'Vipin'
//     }
// });

const UserData = {
    data() {
      return {
            name: 'Vipin',
            country: 'India',
            website: 'https://v3.vuejs.org/',
            website2: '<a href="https://v3.vuejs.org/">VueJs</a>',
            count: 20
      }
    },

    methods: {

      greet(time) {
        // `this` will refer to the component instance
       return 'Good '+ time;
      },

      add() {
        this.count++;
      },

      sub() {
        this.count--;
      }

    }
  }
  
  Vue.createApp(UserData).mount('#vue-app')